﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    public static class RabbitMQHelper
    {
        public static bool TestConnection()
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                var connection = factory.CreateConnection();
                connection.Close();
                return true;
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException)
            {
                return false;
            }
        }
    }
}
