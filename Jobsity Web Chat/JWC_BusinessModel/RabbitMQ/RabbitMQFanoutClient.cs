﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace JWC_BusinessModel
{
    internal class RabbitMQFanoutClient<T> : IDisposable
    {
        private readonly IConnection connection;

        public RabbitMQFanoutClient(string exchange, Action<T> onReceived)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            this.connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange, ExchangeType.Fanout);
            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                              exchange: exchange,
                              routingKey: "");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                var response = JsonConvert.DeserializeObject<T>(message);
                onReceived(response);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: true,
                consumer: consumer);
        }

        public void Dispose()
        {
            this.connection.Close();
        }
    }
}