﻿using System;
using System.Runtime.Serialization;

namespace JWC_BusinessModel
{
    [Serializable]
    internal class RabbitMQClientTimeoutException : Exception
    {
        public RabbitMQClientTimeoutException()
        {
        }

        public RabbitMQClientTimeoutException(string message) : base(message)
        {
        }

        public RabbitMQClientTimeoutException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RabbitMQClientTimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}