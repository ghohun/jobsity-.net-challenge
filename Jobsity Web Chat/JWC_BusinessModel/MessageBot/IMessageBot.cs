﻿using JWC_BusinessModel.MessageBot;
using JWC_DataModel;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    internal interface IMessageBot
    {
        /// <summary>
        /// Checks wheter the bot can process the command. Being async allows for checking for the logic on decoupled bots.
        /// </summary>
        /// <param name="message">The original message received at the chat room, the <seealso cref="Message.Content"/> field contains the command sent by the user</param>
        /// <returns></returns>
        Task<bool> CommandIsForBotAsync(Message message);

        /// <summary>
        /// Process the command. The final response should be sent using RabbitMQ botresponses fanout exchange. 
        /// </summary>
        /// <param name="entityToAdd">The message containing the command on the <seealso cref="Message.Content"/> field</param>
        /// <returns></returns>
        void ProcessCommand(Message entityToAdd);
    }
}