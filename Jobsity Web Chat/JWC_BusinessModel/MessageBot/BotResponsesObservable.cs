﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace JWC_BusinessModel.MessageBot
{
    /// <summary>
    /// An implementation of the Observable pattern, that allows to be informed when a botresponse arrives.
    /// </summary>
    public class BotResponsesObservable : IObservable<BotResponse>, IDisposable
    {
        public static BotResponsesObservable Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BotResponsesObservable();
                }
                return instance;
            }
        }
        private List<IObserver<BotResponse>> observers;
        private RabbitMQFanoutClient<BotResponse> client;
        private static BotResponsesObservable instance;

        private BotResponsesObservable()
        {
            observers = new List<IObserver<BotResponse>>();
            if (RabbitMQHelper.TestConnection())
            {
                this.client = new RabbitMQFanoutClient<BotResponse>("botresponses", (message) =>
                {
                    this.InformNewBotResponse(message);
                });
            }
        }

        /// <summary>
        /// Subscribes the observer to the current observable
        /// </summary>
        /// <param name="observer"></param>
        /// <returns>A <see cref="Unsubscriber{T}"/> object that desubscribes when disposed</returns>
        public IDisposable Subscribe(IObserver<BotResponse> observer)
        {
            // Check whether observer is already registered. If not, add it
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
            return new Unsubscriber<BotResponse>(observers, observer);
        }
        internal void InformNewBotResponse(BotResponse response)
        {
            foreach (var observer in observers)
            {
                observer.OnNext(response);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar llamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.client.Dispose();
                }

                disposedValue = true;
            }
        }

        // TODO: reemplace un finalizador solo si el anterior Dispose(bool disposing) tiene código para liberar los recursos no administrados.
        // ~BotResponsesObservable()
        // {
        //   // No cambie este código. Coloque el código de limpieza en el anterior Dispose(colocación de bool).
        //   Dispose(false);
        // }

        // Este código se agrega para implementar correctamente el patrón descartable.
        public void Dispose()
        {
            // No cambie este código. Coloque el código de limpieza en el anterior Dispose(colocación de bool).
            Dispose(true);
            // TODO: quite la marca de comentario de la siguiente línea si el finalizador se ha reemplazado antes.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
