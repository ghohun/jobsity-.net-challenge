﻿using JWC_DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    internal static class MessageBotFactory
    {
        private static IEnumerable<IMessageBot> availableBots;

        internal static IEnumerable<IMessageBot> AvailableBots
        {
            get
            {
                if(availableBots == null)
                {
                    availableBots = new List<IMessageBot>() { new RabbitMQBot("stockBot") };
                }
                return availableBots;
            }
        }

        internal static async Task ProcessCommandAsync(Message entityToAdd)
        {
            var bots = await GetAsync(entityToAdd);
            foreach (var bot in bots)
            {
                bot.ProcessCommand(entityToAdd);
            }
        }

        internal static void AddAvailableBot(IMessageBot bot)
        {
            (AvailableBots as List<IMessageBot>).Add(bot);
        }

        internal static Task<IEnumerable<IMessageBot>> GetAsync(Message message)
        {
            List<Task<IMessageBot>> tasks = new List<Task<IMessageBot>>();
            foreach (var bot in MessageBotFactory.AvailableBots)
            {
                tasks.Add(bot.CommandIsForBotAsync(message).ContinueWith((result) => { 
                    return result.Result ? bot : null; }));
            }
            //IEnumerable<Task<IMessageBot>> botsForCommand = MessageBotFactory.AvailableBots.Select(async x => await x.CommandIsForBotAsync(message).ContinueWith(y=>y.Result?x:null));
            var mainTask = Task.WhenAll(tasks).ContinueWith<IEnumerable<IMessageBot>>((x)=> {
                var result = x.Result.Where(y =>y!= null).ToList();
                if(result.Count == 0) { 
                    result.Add(new NotSupportedCommandBot());
                }
                return result;
            });
            return mainTask;
        }
    }
}