﻿using JWC_BusinessModel.MessageBot;
using JWC_DataModel;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    /// <summary>
    /// Generic RabbitMQBot manager. Allows to connecto to a bot that hosts a RPC server in the given queue
    /// </summary>
    internal class RabbitMQBot : IMessageBot
    {
        private readonly string queue;

        public RabbitMQRPCClient client { get; }

        public RabbitMQBot(string queue)
        {
            this.queue = queue;
            this.client = new RabbitMQRPCClient(this.queue);            
        }

        public Task<bool> CommandIsForBotAsync(Message message)
        {
            return Task.Factory.StartNew<bool>(()=> { 
                var serializedRequest = JsonConvert.SerializeObject(new RabbitMQBotRequest() { Instruction = Instruction.ValidateCommand, Message = message });
                try { 
                    var serializedResponse = this.client.Call(serializedRequest);
                    return JsonConvert.DeserializeObject<bool>(serializedResponse);
                }
                catch (RabbitMQClientTimeoutException)
                {
                    this.informError($"Could not connect to the {this.queue}, ensure the bot service is up and running", message.BelongsTo_refid);
                    return false;
                }
        });
        }

        public void ProcessCommand(Message message)
        {
            var serializedRequest = JsonConvert.SerializeObject(new RabbitMQBotRequest() { Instruction = Instruction.Process, Message = message });
            try { 
                var serializedResponse = this.client.Call(serializedRequest);
                var response = JsonConvert.DeserializeObject<bool>(serializedResponse);
                if (response == false)
                {
                    this.informError($"There was an error when procesing the command sent to the {this.queue} queue.", message.BelongsTo_refid);
                    //TODO: ProcessCommand failed, define how to treat the failure
                }
            }
            catch (RabbitMQClientTimeoutException){
                this.informError($"Could not connect to the {this.queue} queue, ensure the bot service is up and running",message.BelongsTo_refid);
            }
        }

        private void informError(string message,int chatroomId)
        {
            BotResponse botResponse = new BotResponse()
            {
                Bot = "Rabbit MQ Bot",
                Response = message,
                ChatRoomId = chatroomId
            };
            BotResponsesHelper.Send(botResponse);
        }

        enum Instruction
        {
            Process,
            ValidateCommand
        }

        class RabbitMQBotRequest
        {
            public Instruction Instruction { get; set; }
            public Message Message { get; set; }
        }
    }
}