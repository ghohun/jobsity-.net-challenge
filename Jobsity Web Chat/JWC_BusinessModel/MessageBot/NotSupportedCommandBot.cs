﻿using JWC_BusinessModel.MessageBot;
using JWC_DataModel;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    internal class NotSupportedCommandBot : IMessageBot
    {
        public NotSupportedCommandBot()
        {
        }

        public Task<bool> CommandIsForBotAsync(Message message)
        {
            return new Task<bool>(()=> { 
                return false;
            });
        }

        public void ProcessCommand(Message entityToAdd)
        {
            BotResponse botResponse = new BotResponse()
            {
                Bot = "Command not supported Bot",
                Response = $"Command \"{entityToAdd.Content}\" is not supported",
                ChatRoomId = entityToAdd.BelongsTo_refid
            };
            BotResponsesHelper.Send(botResponse);
        }
    }
}