﻿namespace JWC_BusinessModel.MessageBot
{
    public class BotResponse
    {
        public int ChatRoomId { get; set; }

        public string Response { get; set; }

        public string Bot { get; set; }
    }
}