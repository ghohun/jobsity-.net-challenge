﻿using System;
using System.Collections.Generic;

namespace JWC_BusinessModel.MessageBot
{
    internal class Unsubscriber<T> : IDisposable
    {
        private List<IObserver<BotResponse>> observers;
        private IObserver<BotResponse> observer;

        public Unsubscriber(List<IObserver<BotResponse>> observers, IObserver<BotResponse> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (observers.Contains(observer))
                observers.Remove(observer);
        }
    }
}