﻿using System;

namespace JWC_BusinessModel
{
    public class EntityDeletedEventArgs<TEntity> : EventArgs
    {
        public EntityDeletedEventArgs(TEntity entityDeleted)
        {
            EntityDeleted = entityDeleted;
        }

        public TEntity EntityDeleted { get; set; }
    }
}