﻿using JWC_DataModel;
using System.Threading;

namespace JWC_BusinessModel
{
    internal class MessageRepository : GenericRepository<Message>
    {
        public MessageRepository(UOW uOW) : base(uOW)
        {
        }

        public override Message Add(Message entityToAdd)
        {
            if (entityToAdd.Content.StartsWith("/"))
            {//It is a command, we should call the bot in charge of process it
                if (RabbitMQHelper.TestConnection())
                {
                    new Thread(async () => { await MessageBotFactory.ProcessCommandAsync(entityToAdd); }).Start();
                }
                return entityToAdd;
            }
            else
            {
                return base.Add(entityToAdd);
            }
        }
    }
}