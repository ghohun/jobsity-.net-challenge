﻿using System;

namespace JWC_BusinessModel
{
    public class EntityUpdatedEventArgs<TEntity> : EventArgs
    {
        public EntityUpdatedEventArgs(TEntity entityUpdated)
        {
            EntityUpdated = entityUpdated;
        }

        public TEntity EntityUpdated { get; set; }
    }
}