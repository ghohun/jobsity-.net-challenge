﻿namespace JWC_BusinessModel
{
    public interface ILogicallyDeletable
    {
        bool Active { get; set; }
    }
}