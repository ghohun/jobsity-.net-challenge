﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace JWC_BusinessModel
{
    public interface IListConfiguration<TEntity>
    {
        int? StartIndex { get; }
        int? PageSize { get; }
        Expression<Func<TEntity, bool>> Filter { get; }
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> OrderBy { get; }
        string Includes { get; }
    }
}