﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Returns the results count of the data set that satisfies the given condition
        /// </summary>
        /// <param name="condition">The condition that should be used to filter the dataset</param>
        /// <returns>The resultset count</returns>
        int Count(Expression<Func<TEntity, bool>> condition = null);

        /// <summary>
        /// Allows to obtain a list of the entities, filtered, ordered and paginated with the given configuration
        /// </summary>
        /// <param name="configuration">The configuration to apply</param>
        /// <param name="total">The count of the resultset</param>
        /// <returns>The list of resulting items</returns>
        IEnumerable<TEntity> List(IListConfiguration<TEntity> configuration, out int total);

        /// <summary>
        /// A mapped version of the <see cref="List(IListConfiguration{TEntity}, out int)"/> method
        /// </summary>
        /// <typeparam name="TMappedEntity">The resulting entity type after the mapping is done</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TEntity"/> to a <see cref="TMappedEntity"/></typeparam>
        /// <param name="configuration"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IEnumerable<TMappedEntity> List<TMappedEntity, TMapperProfile>(IListConfiguration<TEntity> configuration, out int total) where TMapperProfile : Profile, new();

        /// <summary>
        /// Filters the data set with the condition given, and apply the includes requested
        /// </summary>
        /// <param name="condition">The condition that should filter the result</param>
        /// <param name="includes">A list of comma separated strings that identify each of the properties and relations that should be included in the resultset.</param>
        /// <returns>The corresponding entity with the given includes</returns>
        TEntity Find(Expression<Func<TEntity, bool>> condition, string includes = null);

        /// <summary>
        /// Mapped version of <see cref="Find(Expression{Func{TEntity, bool}}, string)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The resulting entity type after the mapping is done</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TEntity"/> to a <see cref="TMappedEntity"/></typeparam>
        /// <param name="condicion"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        TMappedEntity Find<TMappedEntity, TMapperProfile>(Expression<Func<TEntity, bool>> condicion, string includes = null) where TMapperProfile : Profile, new();
        
        /// <summary>
        /// Returns the entity that has the given id
        /// </summary>
        /// <param name="id">The desired entity id</param>
        /// <param name="collectionIncludes">A string representing a collection to include</param>
        /// <param name="referenceIncludes">A string representing a reference to include</param>
        /// <returns>The entity with the given id</returns>
        TEntity FindById(object id, String collectionIncludes = null, string referenceIncludes = null);

        /// <summary>
        /// Mapped version of <see cref="FindById(object, string, string)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The resulting entity type after the mapping is done</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TEntity"/> to a <see cref="TMappedEntity"/></typeparam>
        /// <param name="id"></param>
        /// <param name="collectionIncludes"></param>
        /// <param name="referenceIncludes"></param>
        /// <returns></returns>
        TMappedEntity FindById<TMappedEntity, TMapperProfile>(object id, String collectionIncludes = null, string referenceIncludes = null) where TMapperProfile : Profile, new();

        /// <summary>
        /// Event triggered when an entity is added
        /// </summary>
        event EventHandler<EntityAddedEventArgs<TEntity>> EntityAdded;

        /// <summary>
        /// Adds an entity to the data set
        /// </summary>
        /// <param name="entity">The entity to add</param>
        /// <returns>The added entity</returns>
        TEntity Add(TEntity entity);

        /// <summary>
        /// Mapped version of <see cref="Add(TEntity)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The entity type from which the entity should be mapped</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TEntity"/> to a <see cref="TMappedEntity"/> and viceversa</typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        TMappedEntity Add<TMappedEntity, TMapperProfile>(TMappedEntity entity) where TMapperProfile : Profile, new();

        /// <summary>
        /// Event triggered when an entity is deleted
        /// </summary>
        event EventHandler<EntityDeletedEventArgs<TEntity>> EntityDeleted;

        /// <summary>
        /// Deletes an entity with the given id from the data set
        /// </summary>
        /// <param name="id">An object that represents the id of the entity to delete</param>
        void Delete(object id);

        /// <summary>
        /// Removes the given entity from the data set
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Mapped version of <see cref="Delete(TEntity)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The entity type from which the entity should be mapped</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TMappedEntity"/> to a <see cref="TEntity"/></typeparam>
        /// <param name="entity"></param>
        void Delete<TMappedEntity, TMapperProfile>(TMappedEntity entity) where TMapperProfile : Profile, new();

        void LogicDelete<TDeletableEntity>(object id) where TDeletableEntity : ILogicallyDeletable, TEntity;

        /// <summary>
        /// Marks the given entity as deleted by setting the <see cref="ILogicallyDeletable.Active"/> property to false
        /// </summary>
        /// <typeparam name="TDeletableEntity">The type of the entity to disable, which must implement the <see cref="ILogicallyDeletable"/> Interface</typeparam>
        /// <param name="entity">The entity to disable</param>
        void LogicDelete<TDeletableEntity>(TDeletableEntity entity) where TDeletableEntity : TEntity, ILogicallyDeletable;

        /// <summary>
        /// Mapped version of <see cref="LogicDelete{TDeletableEntity}(TDeletableEntity)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The entity type from which the entity should be mapped</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TMappedEntity"/> to a <see cref="TEntity"/></typeparam>
        /// <param name="entity"></param>
        void LogicDelete<TDeletableEntity, TMappedEntity, TMapperProfile>(TMappedEntity entity) where TMapperProfile : Profile, new() where TDeletableEntity : TEntity, ILogicallyDeletable;

        /// <summary>
        /// Event triggered when an entity is updated
        /// </summary>
        event EventHandler<EntityUpdatedEventArgs<TEntity>> EntityUpdated;

        /// <summary>
        /// Updates the entity in the data set
        /// </summary>
        /// <param name="entity">The entity to update</param>
        void Update(TEntity entity);

        /// <summary>
        /// Mapped version of <see cref="Update(TEntity)"/>
        /// </summary>
        /// <typeparam name="TMappedEntity">The entity type from which the entity should be mapped</typeparam>
        /// <typeparam name="TMapperProfile">The AutoMapper profile that should be used to map a <see cref="TMappedEntity"/> to a <see cref="TEntity"/></typeparam>
        /// <param name="entidad"></param>
        void Update<TMappedEntity, TMapperProfile>(TMappedEntity entidad) where TMapperProfile : Profile, new();
    }
}
