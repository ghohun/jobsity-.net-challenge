﻿using AutoMapper;
using JWC_BusinessModel.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    /// <summary>
    /// A Generic implementation of the <see cref="IRepository{TEntity}"/> with basic implementations for Entity Framework
    /// </summary>
    /// <typeparam name="TEntity">The entity that the repository works with</typeparam>
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// The Unit of work to which this repository belongs. This relation allows the repository to use other repositories that exist on the unit of work, and share the EFDBContext with them.
        /// </summary>
        protected UOW UOW { get; set; }
        private DbSet<TEntity> _dbSet;

        public event EventHandler<EntityAddedEventArgs<TEntity>> EntityAdded;
        public event EventHandler<EntityDeletedEventArgs<TEntity>> EntityDeleted;
        public event EventHandler<EntityUpdatedEventArgs<TEntity>> EntityUpdated;

        /// <summary>
        /// Should be called from subclasses when the <see cref="EntityAdded"/> event should rise
        /// </summary>
        /// <param name="eventArgs">The eventargs related to the event</param>
        protected virtual void OnEntityAdded(EntityAddedEventArgs<TEntity> eventArgs)
        {
            EntityAdded?.Invoke(this,eventArgs);
        }

        /// <summary>
        /// Should be called from subclasses when the <see cref="EntityDeleted"/> event should rise
        /// </summary>
        /// <param name="eventArgs">The eventargs related to the event</param>
        protected virtual void OnEntityDeleted(EntityDeletedEventArgs<TEntity> eventArgs)
        {
            EntityDeleted?.Invoke(this, eventArgs);
        }

        /// <summary>
        /// Should be called from subclasses when the <see cref="EntityUpdated"/> event should rise
        /// </summary>
        /// <param name="eventArgs">The eventargs related to the event</param>
        protected virtual void OnEntityUpdated(EntityUpdatedEventArgs<TEntity> eventArgs)
        {
            EntityUpdated?.Invoke(this, eventArgs);
        }

        /// <summary>
        /// The entity set that is being used by this repository
        /// </summary>
        protected DbSet<TEntity> DbSet
        {
            get
            {
                if (_dbSet == null)
                {
                    _dbSet = UOW.Context.Set<TEntity>();
                }
                return _dbSet;
            }
            set
            {
                _dbSet = value;
            }
        }

        /// <summary>
        /// Creates a new repository for the <seealso cref="TEntity"/> defined
        /// </summary>
        /// <param name="uow">The unit of work to which this repository belongs</param>
        public GenericRepository(UOW uow)
        {
            this.UOW = uow;
        }

        #region Repository Methods

        public virtual int Count(Expression<Func<TEntity, bool>> condition = null)
        {
            IQueryable<TEntity> query = DbSet;
            if (condition != null)
            {
                query = query.Where(condition);
            }
            return query.Count();
        }

        public virtual TEntity Add(TEntity entityToAdd)
        {
            DbSet.Add(entityToAdd);
            ApplyAutoSave();
            OnEntityAdded(new EntityAddedEventArgs<TEntity>(entityToAdd) );
            return entityToAdd;
        }

        public virtual void LogicDelete<TDeletableEntity>(object id) where TDeletableEntity : ILogicallyDeletable, TEntity
        {
            TEntity entity = DbSet.Find(id);
            if (entity is ILogicallyDeletable)
            {
                if (entity is TDeletableEntity)
                {
                    ((TDeletableEntity)entity).Active = false;
                    ApplyAutoSave();
                }
                else
                {
                    throw new ArgumentException("Wrong entity type");
                }
            }
            else
            {
                throw new ArgumentException("Entity is not locally deletable");
            }
        }

        public virtual void LogicDelete<TDeletableEntity>(TDeletableEntity entityToDelete) where TDeletableEntity : TEntity, ILogicallyDeletable
        {
            DbSet.Attach(entityToDelete);
            System.Data.Entity.Infrastructure.DbEntityEntry<TEntity> dbEntityEntry = this.UOW.Context.Entry(entityToDelete as TEntity);
            dbEntityEntry.Reload();//To avoid any modification that came with the entity.
            entityToDelete.Active = false;
            dbEntityEntry.State = EntityState.Modified;
            ApplyAutoSave();
        }

        public virtual void Delete(object id)
        {
            TEntity entity = DbSet.Find(id);
            Delete(entity);
            ApplyAutoSave();
            OnEntityDeleted(new EntityDeletedEventArgs<TEntity>(entity) );
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (this.UOW.Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
            ApplyAutoSave();
            OnEntityDeleted(new EntityDeletedEventArgs<TEntity>(entityToDelete) );
        }

        public virtual TEntity Find(Expression<Func<TEntity, bool>> condition, string includes = null)
        {
            return GetQueryWithIncludes(DbSet,includes).Where(condition).FirstOrDefault();
        }

        public virtual TEntity FindById(object id, string collectionIncludes = null, string referenceIncludes = null)
        {
            TEntity entity = DbSet.Find(id);
            if (String.IsNullOrEmpty(collectionIncludes) == false)
            {
                this.UOW.Context.Entry(entity).Collection(collectionIncludes).Load();
            }
            if (String.IsNullOrEmpty(referenceIncludes) == false)
            {
                this.UOW.Context.Entry(entity).Reference(referenceIncludes).Load();
            }
            return entity;
        }

        protected virtual IQueryable<TEntity> GetQueryWithIncludes(IQueryable<TEntity> query, string includes) {
            if (!String.IsNullOrEmpty(includes))
            {
                foreach (var include in includes.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(include);
                }
            }
            return query;
        }

        public virtual IEnumerable<TEntity> List(IListConfiguration<TEntity> configuration, out int total)
        {
            IQueryable<TEntity> list = QueryAndOrderBy(configuration.Filter, configuration.OrderBy, configuration.Includes);
            total = list.Count();
            if (configuration.StartIndex.HasValue && configuration.PageSize.HasValue)
            {
                return list.Paginate(configuration.StartIndex.Value, configuration.PageSize.Value).ToList();
            }
            return list.ToList();
        }

        protected virtual IQueryable<TEntity> QueryAndOrderBy(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, string includes)
        {
            IQueryable<TEntity> query = DbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (!String.IsNullOrEmpty(includes))
            {
                foreach (var include in includes.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(include);
                }
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            return query;
        }

        public virtual void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            this.UOW.Context.Entry(entity).State = EntityState.Modified;
            ApplyAutoSave();
            OnEntityUpdated(new EntityUpdatedEventArgs<TEntity>(entity) );
        }
        #endregion

        #region Repository Methods Mapped Versions
        public TMappedEntity Add<TMappedEntity, TMapperProfile>(TMappedEntity entityToAdd) where TMapperProfile : Profile, new()
        {
            return MapperHelper.Map<TEntity, TMappedEntity, TMapperProfile>(Add(MapperHelper.Map<TMappedEntity, TEntity, TMapperProfile>(entityToAdd)));
        }

        public void LogicDelete<TEntidadBorrable, TMappedEntity, TMapperProfile>(TMappedEntity entity)
            where TEntidadBorrable : TEntity, ILogicallyDeletable
            where TMapperProfile : Profile, new()
        {
            LogicDelete(MapperHelper.Map<TMappedEntity, TEntidadBorrable, TMapperProfile>(entity));
        }

        public void Delete<TMappedEntity, TMapperProfile>(TMappedEntity entity) where TMapperProfile : Profile, new()
        {
            Delete(MapperHelper.Map<TMappedEntity, TEntity, TMapperProfile>(entity));
        }

        public TMappedEntity Find<TMappedEntity, TMapperProfile>(Expression<Func<TEntity, bool>> condition, string includes = null) where TMapperProfile : Profile, new()
        {
            return MapperHelper.Map<TEntity, TMappedEntity, TMapperProfile>(Find(condition, includes));
        }

        public TMappedEntity FindById<TMappedEntity, TMapperProfile>(object id, string collectionIncludes = null, string referenceIncludes = null) where TMapperProfile : Profile, new()
        {
            return MapperHelper.Map<TEntity, TMappedEntity, TMapperProfile>(FindById(id, collectionIncludes, referenceIncludes));
        }

        public IEnumerable<TMappedEntity> List<TMappedEntity, TMapperProfile>(IListConfiguration<TEntity> listConfiguration, out int total) where TMapperProfile : Profile, new()
        {
            return MapperHelper.Map<TEntity, TMappedEntity, TMapperProfile>(List(listConfiguration, out total));
        }

        public void Update<TMappedEntity, TMapperProfile>(TMappedEntity entityToUpdate) where TMapperProfile : Profile, new()
        {
            Update(MapperHelper.Map<TMappedEntity, TEntity, TMapperProfile>(entityToUpdate));
        }
        #endregion

        private void ApplyAutoSave()
        {
            if (this.UOW.EnableAutoSaveFeature)
            { this.UOW.Context.SaveChanges(); }
        }
    }
}
