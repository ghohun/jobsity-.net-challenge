﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> temp, int StartIndex, int PageSize)
        {
            if (PageSize != 0)
            {
                temp = temp.Skip(StartIndex).Take(PageSize);
            }
            return temp;
        }
        public static IQueryable<T> Paginate<T>(this IQueryable<T> temp, int StartIndex, int PageSize)
        {
            if (PageSize != 0)
            {
                temp = temp.Skip(StartIndex).Take(PageSize);
            }
            return temp;
        }
    }
}
