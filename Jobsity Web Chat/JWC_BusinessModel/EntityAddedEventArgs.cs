﻿using System;

namespace JWC_BusinessModel
{
    public class EntityAddedEventArgs<TEntity> : EventArgs
    {
        public EntityAddedEventArgs(TEntity entityAdded)
        {
            EntityAdded = entityAdded;
        }

        public TEntity EntityAdded { get; set; }
    }
}