﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JWC_DataModel;

namespace JWC_BusinessModel
{
    public class UOW : IDisposable
    {
        /// <summary>
        /// If set to true, the repositories will perform a SaveChanges after each change to the context.
        /// If set to false, user must run a manual SaveChanges to impact the modifications.
        /// </summary>
        public bool EnableAutoSaveFeature { get; private set; }

        /// <summary>
        /// Constructs a new Unit of work that shares the context between multiple repositories. 
        /// </summary>
        /// <param name="context">The context that this UOW will share with all the repositories</param>
        /// <param name="enableAutoSaveFeature">A starting setting for the auto save feature</param>
        public UOW(EFDBContext context,bool enableAutoSaveFeature = true)
        {
            this.Context = context;
            this.EnableAutoSaveFeature = enableAutoSaveFeature;
        }
        /// <summary>
        /// The context that this UOW will share with all the repositories
        /// </summary>
        public EFDBContext Context { get; private set; }

        /// <summary>
        /// The repository for the <seealso cref="ChatUser"/> entity
        /// </summary>
        public IRepository<ChatUser> UserRepo
        {
            get {
                if(userRepo == null)
                {
                    this.userRepo = new GenericRepository<ChatUser>(this);
                }
                return userRepo;
            }

        }

        /// <summary>
        /// The repository for the <seealso cref="Message"/> entity
        /// </summary>
        public IRepository<Message> MessageRepo
        {
            get
            {
                if (messageRepo == null)
                {
                    this.messageRepo = new MessageRepository(this);
                }
                return messageRepo;
            }
        }

        /// <summary>
        /// The repository for the <seealso cref="Chatroom"/> entity
        /// </summary>
        public IRepository<Chatroom> ChatroomRepo
        {
            get
            {
                if (chatroomRepo == null)
                {
                    this.chatroomRepo = new GenericRepository<Chatroom>(this);
                }
                return chatroomRepo;
            }
        }

        /// <summary>
        /// Save the changes done to the context in all the repositories. If the <seealso cref="EnableAutoSaveFeature"/> is true, the call to this method is not needed
        /// </summary>
        /// <returns><see cref="Context"/></returns>
        public int SaveChanges() {
            return this.Context.SaveChanges();
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        private IRepository<ChatUser> userRepo;
        private IRepository<Message> messageRepo;
        private IRepository<Chatroom> chatroomRepo;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UOW()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
