﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel
{
    public class GenericListConfiguration<TEntity> : IListConfiguration<TEntity>
    {
        public int? StartIndex { get; set; }

        public int? PageSize { get; set; }

        public Expression<Func<TEntity, bool>> Filter { get; set; }

        public Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> OrderBy { get; set; }

        public string Includes { get; set; }
    }
}
