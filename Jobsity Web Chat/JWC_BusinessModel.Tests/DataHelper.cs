﻿using JWC_DataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWC_BusinessModel.Tests
{
    internal static class DataHelper
    {
        internal static UOW GetUOW()
        {
            UOW uOW = new UOW(new EFDBContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.Replace("%AppData%", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
                , true);
            return uOW;
        }

        internal static Chatroom GetNewTestChatroom()
        {
            return new Chatroom()
            {
                Name = "AutomationTestRoom",
                Created_at = DateTime.Now,
                Description = "An automated created chat room for testing"
            };
        }
    }
}
