﻿using System;
using JWC_DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JWC_BusinessModel.Tests
{
    [TestClass]
    public class ChatroomRepo
    {
        [TestMethod]
        public void Add()
        {
            Chatroom created;
            Chatroom testChatroom = DataHelper.GetNewTestChatroom();
            using (var uow = DataHelper.GetUOW())
            {
                created = uow.ChatroomRepo.Add(testChatroom);
                Assert.IsNotNull(created);
                Assert.IsNotNull(uow.ChatroomRepo.FindById(created.Id));
                Assert.AreEqual(created.Name, testChatroom.Name);
                Assert.AreEqual(created.Description, testChatroom.Description);
                Assert.AreEqual(created.Created_at, testChatroom.Created_at);
                uow.ChatroomRepo.Delete(created);
            }
        }
        [TestMethod]
        public void Edit()
        {
            Chatroom created;
            Chatroom testChatroom = DataHelper.GetNewTestChatroom();
            using (var uow = DataHelper.GetUOW())
            {
                created = uow.ChatroomRepo.Add(testChatroom);
                var oldName = created.Name;
                created.Name = created.Name + "NameModification";
                uow.ChatroomRepo.Update(created);
                var dbUpdated = uow.ChatroomRepo.FindById(created.Id);
                Assert.AreEqual(created.Name, dbUpdated.Name);
                Assert.AreNotEqual(dbUpdated.Description,oldName);
                uow.ChatroomRepo.Delete(created);
            }
        }
        [TestMethod]
        public void Delete()
        {
            Chatroom created;
            using (var uow = DataHelper.GetUOW())
            {
                created = uow.ChatroomRepo.Add(DataHelper.GetNewTestChatroom());
                uow.ChatroomRepo.Delete(created);
                Assert.IsNull(uow.ChatroomRepo.FindById(created.Id));
            }
        }
        [TestMethod]
        public void EntityAddedEvent()
        {
            Chatroom created;
            Chatroom testChatroom = DataHelper.GetNewTestChatroom();
            using (var uow = DataHelper.GetUOW())
            {
                bool reached = false;
                uow.ChatroomRepo.EntityAdded += (src,e) => {
                    reached = true;
                };
                created = uow.ChatroomRepo.Add(testChatroom);
                Assert.IsTrue(reached);
                uow.ChatroomRepo.Delete(created);
            }
        }
        [TestMethod]
        public void EntityDeletedEvent()
        {
            Chatroom created;
            Chatroom testChatroom = DataHelper.GetNewTestChatroom();
            using (var uow = DataHelper.GetUOW())
            {
                created = uow.ChatroomRepo.Add(testChatroom);
                bool reached = false;
                uow.ChatroomRepo.EntityDeleted += (src, e) => {
                    reached = true;
                };
                uow.ChatroomRepo.Delete(created);
                Assert.IsTrue(reached);
            }
        }
        [TestMethod]
        public void EntityUpdatedEvent()
        {
            Chatroom created;
            Chatroom testChatroom = DataHelper.GetNewTestChatroom();
            using (var uow = DataHelper.GetUOW())
            {
                created = uow.ChatroomRepo.Add(testChatroom);
                bool reached = false;
                uow.ChatroomRepo.EntityUpdated += (src, e) => {
                    reached = true;
                };
                created.Name = created.Name + "NameModification";
                uow.ChatroomRepo.Update(created);
                Assert.IsTrue(reached);
                uow.ChatroomRepo.Delete(created);
            }
        }
    }
}
