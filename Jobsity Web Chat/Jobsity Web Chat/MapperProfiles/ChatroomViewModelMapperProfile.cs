﻿using AutoMapper;
using Jobsity_Web_Chat.Models;
using JWC_DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jobsity_Web_Chat.MapperProfiles
{
    public class ChatroomViewModelMapperProfile : Profile
    {
        public ChatroomViewModelMapperProfile()
        {
            MessageViewModelMapperProfile.configure(this);
            configure(this);
        }
        public static void configure(Profile perfil)
        {
            perfil.CreateMap<Chatroom, ChatroomViewModel>();
            perfil.CreateMap<ChatroomViewModel,Chatroom>();
        }
    }
}