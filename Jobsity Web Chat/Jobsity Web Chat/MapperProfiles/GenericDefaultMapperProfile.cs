﻿using AutoMapper;
using Jobsity_Web_Chat.Models;
using JWC_DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jobsity_Web_Chat.MapperProfiles
{
    public class GenericDefaultMapperProfile<TEntity,TMappedEntity> : Profile
    {
        public GenericDefaultMapperProfile()
        {
            configure(this);
        }
        public static void configure(Profile perfil)
        {
            perfil.CreateMap<TEntity, TMappedEntity>();
        }
    }
}