﻿using AutoMapper;
using Jobsity_Web_Chat.Models;
using JWC_DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jobsity_Web_Chat.MapperProfiles
{
    public class MessageViewModelMapperProfile : Profile
    {
        public MessageViewModelMapperProfile()
        {
            configure(this);
        }
        public static void configure(Profile perfil)
        {
            perfil.CreateMap<Message, MessageViewModel>()
                .ForMember(src=>src.AuthorUserName,opt=>opt.MapFrom(dest=>dest.Author.UserName));
            perfil.CreateMap<MessageViewModel, Message>();
        }
    }
}