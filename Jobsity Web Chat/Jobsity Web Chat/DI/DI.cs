﻿using Jobsity_Web_Chat.MapperProfiles;
using Jobsity_Web_Chat.Models;
using Jobsity_Web_Chat.Signal_R;
using JWC_BusinessModel;
using JWC_DataModel;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jobsity_Web_Chat
{
    public static class DI
    {
        private static IHubContext messageHub;

        public static UOW UOW => GetUOW();

        public static IHubContext MessageHub
        {
            get
            {
                if (messageHub == null)
                {
                    messageHub = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
                }
                return messageHub;
            }
        }
        private static UOW GetUOW()
        {
            UOW uOW = new UOW(new EFDBContext(), true);
            uOW.MessageRepo.EntityAdded += MessageRepo_EntityAdded;
            uOW.MessageRepo.EntityDeleted += MessageRepo_EntityDeleted;
            uOW.MessageRepo.EntityUpdated += MessageRepo_EntityUpdated;
            return uOW;
        }

        private static void MessageRepo_EntityUpdated(object sender, EntityUpdatedEventArgs<Message> e)
        {
            UpdateMessagesOnHub(sender, e.EntityUpdated.BelongsTo_refid);
        }

        private static void MessageRepo_EntityDeleted(object sender, EntityDeletedEventArgs<Message> e)
        {
            UpdateMessagesOnHub(sender, e.EntityDeleted.BelongsTo_refid);
        }

        private static void MessageRepo_EntityAdded(object sender, EntityAddedEventArgs<Message> e)
        {
            var repository = (sender as IRepository<Message>);
            var message = repository.Find<MessageViewModel, MessageViewModelMapperProfile>(x=>x.Id == e.EntityAdded.Id,nameof(Message.Author));
            Signal_R.MessageHub.AddMessage(MessageHub, e.EntityAdded.BelongsTo_refid, message);
        }

        private static void UpdateMessagesOnHub(object sender, int chatroomId)
        {
            IEnumerable<MessageViewModel> messages = (sender as IRepository<Message>).List<MessageViewModel, MessageViewModelMapperProfile>(
                new GenericListConfiguration<Message>()
                {
                    Filter = x => x.BelongsTo_refid == chatroomId,
                    Includes = nameof(Message.Author),
                    OrderBy = x => x.OrderByDescending(y => y.Created_at),
                    PageSize = 50,
                    StartIndex = 0
                }, out _);
            Signal_R.MessageHub.UpdateMessages(MessageHub, chatroomId, messages);
        }
    }
}