﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using JWC_DataModel;
using Jobsity_Web_Chat.Models;
using Jobsity_Web_Chat.MapperProfiles;
using JWC_BusinessModel;
using Microsoft.AspNet.Identity;

namespace Jobsity_Web_Chat.Api
{
    [RoutePrefix("api")]
    public class MessageController : ApiController
    {
        // GET: api/Message
        [Authorize]
        public IQueryable<MessageViewModel> GetMessages()
        {
            IEnumerable<MessageViewModel> result;
            using (var uow = DI.UOW)
            {
                result = uow.MessageRepo.List<MessageViewModel, MessageViewModelMapperProfile>(
                    new GenericListConfiguration<Message>() { 
                        OrderBy = x=>x.OrderByDescending(y=>y.Created_at),
                        PageSize = 50,
                        StartIndex = 0
                    }
                    , out int total);
            };
            return result.AsQueryable();
        }

        /// <summary>
        /// Gets the 50 last messages written in the chatroom with the given id
        /// </summary>
        /// <param name="chatroomId">The id of the chatroom</param>
        /// <returns>The 50 last messages of the chatroom</returns>
        [Authorize]
        public IQueryable<MessageViewModel> GetMessagesForChatRoom(int chatroomId)
        {
            IEnumerable<MessageViewModel> result;
            using (var uow = DI.UOW)
            {
                result = uow.MessageRepo.List<MessageViewModel, MessageViewModelMapperProfile>(
                    new GenericListConfiguration<Message>() { 
                        Filter = x=>x.BelongsTo_refid == chatroomId,
                        OrderBy = x => x.OrderByDescending(y => y.Created_at),
                        PageSize = 50,
                        StartIndex = 0
                    }, 
                    out int total);
            };
            return result.AsQueryable();
        }

        // GET: api/Message/5
        [ResponseType(typeof(MessageViewModel))]
        [Authorize]
        public IHttpActionResult GetMessage(int id)
        {
            MessageViewModel messageViewModel;
            using (var uow = DI.UOW)
            {
                messageViewModel = uow.MessageRepo.FindById<MessageViewModel, MessageViewModelMapperProfile>(id);
            };
            if (messageViewModel == null)
            {
                return NotFound();
            }

            return Ok(messageViewModel);
        }

        // PUT: api/Message/5
        [ResponseType(typeof(void))]
        [Authorize]
        public IHttpActionResult PutMessage(int id, MessageViewModel messageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != messageViewModel.Id)
            {
                return BadRequest();
            }

            try
            {
                using (var uow = DI.UOW)
                {
                    uow.MessageRepo.Update<MessageViewModel, MessageViewModelMapperProfile>(messageViewModel);
                };
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Message
        [Route("Message",Name = "ApiNewMessage")]
        [ResponseType(typeof(MessageViewModel))]
        [Authorize]
        public IHttpActionResult PostMessage(MessageViewModel messageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            messageViewModel.Author_refid = User.Identity.GetUserId();
            messageViewModel.Created_at = DateTime.Now;

            using (var uow = DI.UOW)
            {
                uow.MessageRepo.Add<MessageViewModel, MessageViewModelMapperProfile>(messageViewModel);
            };

            return Ok(messageViewModel);
        }

        // DELETE: api/Message/5
        [ResponseType(typeof(MessageViewModel))]
        [Authorize]
        public IHttpActionResult DeleteMessage(int id)
        {
            MessageViewModel message;
            using (var uow = DI.UOW)
            {
                message = uow.MessageRepo.FindById<MessageViewModel, MessageViewModelMapperProfile>(id);
            };
            if (message == null)
            {
                return NotFound();
            }

            using (var uow = DI.UOW)
            {
                uow.MessageRepo.Delete<MessageViewModel, MessageViewModelMapperProfile>(message);
            };

            return Ok(message);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int id)
        {
            using (var uow = DI.UOW)
            {
                return uow.MessageRepo.Count(e => e.Id == id) > 0;
            };
        }
    }
}