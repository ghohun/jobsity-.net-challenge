﻿using System.Web;
using System.Web.Mvc;

namespace Jobsity_Web_Chat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
