﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jobsity_Web_Chat.Models
{
    public class ChatroomViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Created_at { get; set; }

        public ICollection<MessageViewModel> Messages { get; set; }
    }
}