﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jobsity_Web_Chat.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<MessageViewModel> Messages { get; set; }
    }
}