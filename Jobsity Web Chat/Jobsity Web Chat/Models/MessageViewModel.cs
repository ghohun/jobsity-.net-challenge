﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jobsity_Web_Chat.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(int.MaxValue,MinimumLength = 1)]
        public string Content { get; set; }

        public DateTime Created_at { get; set; }
        
        public int BelongsTo_refid { get; set; }

        public string Author_refid { get; set; }

        public string AuthorUserName { get; set; }
    }
}