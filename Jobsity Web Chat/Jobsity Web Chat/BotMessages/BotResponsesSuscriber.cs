﻿using Jobsity_Web_Chat.Models;
using JWC_BusinessModel.MessageBot;
using System;

namespace Jobsity_Web_Chat
{
    internal class BotResponsesSuscriber : IObserver<BotResponse>,IDisposable
    {
        public static BotResponsesSuscriber Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BotResponsesSuscriber();
                }
                return instance;
            }
        }
        private IDisposable unsuscriber;
        private static BotResponsesSuscriber instance;

        public void Dispose()
        {
            if (this.unsuscriber != null)
            {
                this.unsuscriber.Dispose();
            }            
        }

        public void Suscribe() {
            this.unsuscriber = BotResponsesObservable.Instance.Subscribe(this);
        }

        public void OnNext(BotResponse response)
        {
            Signal_R.MessageHub.AddMessage(DI.MessageHub, response.ChatRoomId, new MessageViewModel()
            {
                AuthorUserName = response.Bot,
                Content = response.Response,
                BelongsTo_refid = response.ChatRoomId,
                Created_at = DateTime.Now
            });
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}