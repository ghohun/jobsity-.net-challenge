﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Jobsity_Web_Chat.Startup))]
namespace Jobsity_Web_Chat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
