﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;
using JWC_DataModel;
using Jobsity_Web_Chat.Models;
using JWC_BusinessModel;
using Jobsity_Web_Chat.MapperProfiles;

namespace Jobsity_Web_Chat.Controllers
{
    public class ChatroomController : Controller
    {
        // GET: Chatroom
        [Authorize]
        public ActionResult Index()
        {
            IEnumerable<ChatroomViewModel> result;
            using (var uow = DI.UOW)
            {
                result = uow.ChatroomRepo.List<ChatroomViewModel, ChatroomViewModelMapperProfile>(new GenericListConfiguration<Chatroom>(), out int total);
            };
            return View(result);
        }

        // GET: Chatroom/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatroomViewModel chatroom;
            using (var uow = DI.UOW)
            {
                chatroom = uow.ChatroomRepo.Find<ChatroomViewModel, ChatroomViewModelMapperProfile>(x=>x.Id == id.Value, $"{nameof(Chatroom.Messages)}.{nameof(Message.Author)}");
            };
            if (chatroom == null)
            {
                return HttpNotFound();
            }
            return View(chatroom);
        }

        // GET: Chatroom/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Chatroom/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Created_at")] ChatroomViewModel chatroomViewModel)
        {
            if (ModelState.IsValid)
            {
                chatroomViewModel.Created_at = DateTime.Now;
                using (var uow = DI.UOW)
                {
                    uow.ChatroomRepo.Add<ChatroomViewModel, ChatroomViewModelMapperProfile>(chatroomViewModel);
                };
                return RedirectToAction("Index");
            }

            return View(chatroomViewModel);
        }

        // GET: Chatroom/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatroomViewModel chatroom;
            using (var uow = DI.UOW)
            {
                chatroom = uow.ChatroomRepo.FindById<ChatroomViewModel, ChatroomViewModelMapperProfile>(id.Value);
            };
            if (chatroom == null)
            {
                return HttpNotFound();
            }
            return View(chatroom);
        }

        // POST: Chatroom/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Created_at")] ChatroomViewModel chatroomViewModel)
        {
            if (ModelState.IsValid)
            {
                Chatroom DBChatRoom;
                using (var uow = DI.UOW)
                {
                    DBChatRoom = uow.ChatroomRepo.FindById(chatroomViewModel.Id);
                };
                chatroomViewModel.Created_at = DBChatRoom.Created_at;
                using (var uow = DI.UOW)
                {
                    uow.ChatroomRepo.Update<ChatroomViewModel, ChatroomViewModelMapperProfile>(chatroomViewModel);
                };
                return RedirectToAction("Index");
            }
            return View(chatroomViewModel);
        }

        // GET: Chatroom/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatroomViewModel chatroom;
            using (var uow = DI.UOW)
            {
                chatroom = uow.ChatroomRepo.FindById<ChatroomViewModel, ChatroomViewModelMapperProfile>(id.Value);
            };
            if (chatroom == null)
            {
                return HttpNotFound();
            }
            return View(chatroom);
        }

        // POST: Chatroom/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var uow = DI.UOW)
            {
                uow.ChatroomRepo.Delete(id);
            };
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}
