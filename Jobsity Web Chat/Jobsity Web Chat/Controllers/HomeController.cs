﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jobsity_Web_Chat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult About()
        {
            ViewBag.Message = "Jobsity Web Chat Challenge by Lucas Marc.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact the developer";

            return View();
        }
    }
}