﻿using Jobsity_Web_Chat.MapperProfiles;
using Jobsity_Web_Chat.Models;
using JWC_BusinessModel;
using JWC_DataModel;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace Jobsity_Web_Chat.Controllers
{
    public class UserController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            IEnumerable<UserViewModel> result;
            using (var uow = DI.UOW) {
                result = uow.UserRepo.List<UserViewModel, GenericDefaultMapperProfile<ChatUser, UserViewModel>>(new GenericListConfiguration<ChatUser>(), out int total);
            };
            return View(result);
        }

        // GET: User/Details/5
        [Authorize]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserViewModel User;
            using (var uow = DI.UOW)
            {
                User = uow.UserRepo.Find<UserViewModel, GenericDefaultMapperProfile<ChatUser, UserViewModel>>(x => x.Id == id, $"{nameof(ChatUser.Messages)}.{nameof(Message.Author)}");
            };
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return RedirectToActionPermanent(actionName:nameof(AccountController.Register),controllerName: nameof(AccountController));
        }

        // GET: User/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserViewModel User;
            using (var uow = DI.UOW)
            {
                User = uow.UserRepo.FindById<UserViewModel, GenericDefaultMapperProfile<ChatUser, UserViewModel>>(id.Value);
            };
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EmailAddress")] UserViewModel UserViewModel)
        {
            if (ModelState.IsValid)
            {
                using (var uow = DI.UOW)
                {
                    uow.UserRepo.Update<UserViewModel, GenericDefaultMapperProfile<UserViewModel,ChatUser>>(UserViewModel);
                };
                return RedirectToAction("Index");
            }
            return View(UserViewModel);
        }

        // GET: User/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserViewModel User;
            using (var uow = DI.UOW)
            {
                User = uow.UserRepo.FindById<UserViewModel, GenericDefaultMapperProfile<ChatUser,UserViewModel>>(id.Value);
            };
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var uow = DI.UOW)
            {
                uow.UserRepo.Delete(id);
            };
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}