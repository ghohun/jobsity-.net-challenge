﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jobsity_Web_Chat.MapperProfiles;
using Jobsity_Web_Chat.Models;
using JWC_BusinessModel;
using JWC_BusinessModel.MessageBot;
using JWC_DataModel;
using Microsoft.AspNet.SignalR;

namespace Jobsity_Web_Chat.Signal_R
{
    public class MessageHub : Hub
    {
        public static void UpdateMessages(IHubContext context, int idChatRoom, IEnumerable<MessageViewModel> messages)
        {
            context.Clients.Group(idChatRoom.ToString()).UpdateMessages(messages);
        }
        public static void AddMessage(IHubContext context, int idChatRoom, MessageViewModel message)
        {
            context.Clients.Group(idChatRoom.ToString()).AddMessage(message);
        }

        public Task<IEnumerable<MessageViewModel>> JoinGroup(string groupName)
        {
            var chatroomId = int.Parse(groupName);
            //When any client joins, we subscribe to the bot reposnes, as there is someone into a chatroom
            BotResponsesSuscriber.Instance.Suscribe();
            return Groups.Add(Context.ConnectionId, groupName).ContinueWith((t) =>
            {
                return DI.UOW.MessageRepo.List<MessageViewModel, MessageViewModelMapperProfile>(
                new GenericListConfiguration<Message>()
                {
                    Filter = x => x.BelongsTo_refid == chatroomId,
                    Includes = nameof(Message.Author),
                    OrderBy = x => x.OrderByDescending(y => y.Created_at),
                    PageSize = 50,
                    StartIndex = 0
                }, out _);
            });
        }

        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }

    }
}