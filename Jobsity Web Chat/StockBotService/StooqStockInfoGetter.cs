﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;

namespace StockBotService
{
    internal class StooqStockInfoGetter
    {
        private string stockName;

        public StooqStockInfoGetter(string stockName)
        {
            this.stockName = stockName;
        }

        internal IEnumerable<StooqStock> GetStocks()
        {
            string url = $"https://stooq.com/q/l/?s={this.stockName}&f=sd2t2ohlcv&h&e=csv";
            string csvContent = GetCSV(url);
            IEnumerable<StooqStock> stocks = GetStocksFromCsvString(csvContent);
            return stocks;
        }
        private static string GetCSV(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();
            sr.Close();

            return results;
        }

        private static IEnumerable<StooqStock> GetStocksFromCsvString(string csvContent)
        {
            List<StooqStock> result = new List<StooqStock>();
            string[] csvLinesWithHeader = csvContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            string[] csvLinesWithoutHeader = new string[csvLinesWithHeader.Length - 1];
            Array.Copy(csvLinesWithHeader, 1, csvLinesWithoutHeader, 0, csvLinesWithHeader.Length - 1);
            foreach (var csvLine in csvLinesWithoutHeader)
            {
                var csvLineArray = csvLine.Split(',');
                if ((csvLineArray[1] == "N/D" 
                    && csvLineArray[2] == "N/D" 
                    && csvLineArray[3] == "N/D" 
                    && csvLineArray[4] == "N/D" 
                    && csvLineArray[5] == "N/D" 
                    && csvLineArray[6] == "N/D" 
                    && csvLineArray[7] == "N/D") == false)
                {//only add result if stooq found it
                    var stock = new StooqStock()
                    {
                        Symbol = csvLineArray[0]
                    };
                    DateTime tmpDate;
                    TimeSpan tmpTimeSpan;
                    Decimal tmpDecimal;
                    int tmpInt;
                    if (DateTime.TryParseExact(csvLineArray[1], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out tmpDate))
                    {
                        stock.Date = tmpDate;
                    }
                    if (TimeSpan.TryParseExact(csvLineArray[2], "HH:mm:ss", CultureInfo.InvariantCulture, out tmpTimeSpan))
                    {
                        stock.Time = tmpTimeSpan;
                    }
                    if (Decimal.TryParse(csvLineArray[3], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tmpDecimal))
                    {
                        stock.Open = tmpDecimal;
                    }
                    if (Decimal.TryParse(csvLineArray[4], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tmpDecimal))
                    {
                        stock.High = tmpDecimal;
                    }
                    if (Decimal.TryParse(csvLineArray[5], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tmpDecimal))
                    {
                        stock.Low = tmpDecimal;
                    }
                    if (Decimal.TryParse(csvLineArray[6], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tmpDecimal))
                    {
                        stock.Close = tmpDecimal;
                    }
                    if (int.TryParse(csvLineArray[7], out tmpInt))
                    {
                        stock.Volume = tmpInt;
                    }
                    result.Add(stock);
                }
            }
            return result;
        }
    }
}