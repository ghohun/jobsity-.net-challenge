﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using StockBotService.RabbitMQ;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StockBotService
{
    public partial class StockBotService : ServiceBase
    {
        public StockBotService()
        {
            InitializeComponent();
        }

        internal RabbitMQRPCServer client { get; private set; }

        protected override void OnStart(string[] args)
        {
            this.client = new RabbitMQRPCServer("stockBot", (serializedRequest) =>
             {
                 var request = JsonConvert.DeserializeObject<RabbitMQBotRequest>(serializedRequest);
                 switch (request.Instruction)
                 {
                     case Instruction.Process:
                         return JsonConvert.SerializeObject(this.ProcessRequest(request.Message));
                     case Instruction.ValidateCommand:
                     default:
                         return JsonConvert.SerializeObject(ValidateCommand(request.Message));
                 }
             });
            this.client.Start();
        }

        private static bool ValidateCommand(Message message)
        {
            return message.Content.ToLower().StartsWith("/stock=");
        }

        private bool ProcessRequest(Message message)
        {
            try
            {
                //We get the stock name from the message
                string stockName = message.Content.ToLower().Replace("/stock=", "");
                //The process of the stock is done in another thread
                new Thread(() =>
                {
                    try
                    {
                        var stocks = new StooqStockInfoGetter(stockName).GetStocks();
                        if (stocks.Count() == 0)
                        {
                            BotResponse botResponse = new BotResponse()
                            {
                                Bot = "Stooq Stock Bot",
                                Response = $"Stock \"{stockName}\" couldn't be found",
                                ChatRoomId = message.BelongsTo_refid
                            };
                            BotResponsesHelper.Send(botResponse);
                        }
                        else
                        {
                            foreach (var stock in stocks)
                            {
                                BotResponse botResponse = new BotResponse()
                                {
                                    Bot = "Stooq Stock Bot",
                                    Response = $"{stock.Symbol} quote is ${stock.Close} per share",
                                    ChatRoomId = message.BelongsTo_refid
                                };
                                BotResponsesHelper.Send(botResponse);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        BotResponse botResponse = new BotResponse()
                        {
                            Bot = "Stooq Stock Bot",
                            Response = $"There was an error when processing the stock command \"{message.Content}\". Exception message: {e.Message}",
                            ChatRoomId = message.BelongsTo_refid
                        };
                        BotResponsesHelper.Send(botResponse);
                    }
                }).Start();
                //We inform that the process was started correctly
                return true;
            }
            catch (Exception e)
            {
                //Something failed when starting the process, we inform that the process could not start.
                return false;
            }
        }

        protected override void OnStop()
        {
            this.client.Stop();
        }


        enum Instruction
        {
            Process,
            ValidateCommand
        }

        class RabbitMQBotRequest
        {
            public Instruction Instruction { get; set; }
            public Message Message { get; set; }
        }
    }
}
