﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace StockBotService
{
    static class BotResponsesHelper
    {
        internal static void Send(BotResponse botResponse)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(botResponse));
                channel.BasicPublish(exchange: "botresponses",
                                     routingKey: "",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}