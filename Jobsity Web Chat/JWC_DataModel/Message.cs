﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JWC_DataModel
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        public string Content { get; set; }

        public DateTime Created_at { get; set; }

        [ForeignKey(nameof(BelongsTo))]
        public int BelongsTo_refid { get; set; }

        public Chatroom BelongsTo { get; set; }

        [ForeignKey(nameof(Author))]
        public string Author_refid { get; set; }

        public ChatUser Author { get; set; }
    }
}
