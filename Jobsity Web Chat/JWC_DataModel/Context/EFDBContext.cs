﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace JWC_DataModel
{
    public class EFDBContext : IdentityDbContext<ChatUser>
    {
        public EFDBContext()
            : this("DefaultConnection")
        {
        }
        public EFDBContext(string connection)
            : base(connection, throwIfV1Schema: false)
        {
        }

        public static EFDBContext Create()
        {
            return new EFDBContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<ApplicationBaseUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<ChatUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
        }

        //public DbSet<ChatUser> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Chatroom> Chatrooms { get; set; }
    }
}
