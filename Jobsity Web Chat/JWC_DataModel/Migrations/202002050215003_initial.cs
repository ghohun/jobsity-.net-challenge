namespace JWC_DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chatrooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        Created_at = c.DateTime(nullable: false),
                        BelongsTo_refid = c.Int(nullable: false),
                        Author_refid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_refid, cascadeDelete: true)
                .ForeignKey("dbo.Chatrooms", t => t.BelongsTo_refid, cascadeDelete: true)
                .Index(t => t.BelongsTo_refid)
                .Index(t => t.Author_refid);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserChatrooms",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Chatroom_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Chatroom_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Chatrooms", t => t.Chatroom_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Chatroom_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "BelongsTo_refid", "dbo.Chatrooms");
            DropForeignKey("dbo.Messages", "Author_refid", "dbo.Users");
            DropForeignKey("dbo.UserChatrooms", "Chatroom_Id", "dbo.Chatrooms");
            DropForeignKey("dbo.UserChatrooms", "User_Id", "dbo.Users");
            DropIndex("dbo.UserChatrooms", new[] { "Chatroom_Id" });
            DropIndex("dbo.UserChatrooms", new[] { "User_Id" });
            DropIndex("dbo.Messages", new[] { "Author_refid" });
            DropIndex("dbo.Messages", new[] { "BelongsTo_refid" });
            DropTable("dbo.UserChatrooms");
            DropTable("dbo.Users");
            DropTable("dbo.Messages");
            DropTable("dbo.Chatrooms");
        }
    }
}
