﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace JWC_DataModel
{
    public class ChatUser : ApplicationBaseUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<Message> Messages { get; set; }
    }
}
