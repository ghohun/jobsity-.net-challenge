﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JWC_DataModel
{
    public class Chatroom
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Created_at { get; set; }

        public ICollection<Message> Messages { get; set; }
    }
}
