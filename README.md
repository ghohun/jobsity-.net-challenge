# Jobsity Web Chat Challenge by Lucas Marc

This project is aimed at the realization of a backend development challenge requested by Jobsity, and carried out by Lucas Marc. It is a web chat application, which allows several users to enter a chat room, exchange messages, and ask a bot for the price of certain shares on the stock exchange.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Despite having contemplated good practices to achieve good scalability and productive operation, the project is not intended for operation in a productive environment, and has been tested only in development environments.

### Description of the software structure

The system is separated into three main layers:

* A data access layer
* A layer of business logic
* A layer oriented to the graphical interface and user interaction with the system.

This structure allows, in the first place, to make a correct separation of responsibilities, and secondly, it offers the possibility of, for example, making a new mobile interface layer, and achieving a version of the system suitable for mobile devices, or easily change the data layer to save application data in the cloud.

Also, an implementation of a bot has been carried out, which allows the user to ask for a specific stock quote, and returns its current close value, as informed by the Stooq API.

### Data Access Layer

The data model uses Entity Framework, and is currently using a connection string to connect to a Microsoft SQL Server Express LocalDB database. This DB file is created in the App_Data folder of the project, which needs to exist. Changing the "DefaultConnection" connection string in the web.config file allows to connect to any SQL database, and it can be easily configured to get it connecting to MariaDB, MySql or other database engines supported by Entity Framework. It is configured to create the database based on the data model in case the DB does not exist, and to map it to the EFDBContext class, which represents the Entity Framework Context.

The model has three main classes:

* Chatroom: The chat rooms on which users can join and write messages.
* Message: The chats that users send to chat rooms.
* ChatUser: Refers to the chat users, who can join any chat room. Inherits from a class called ApplicationBaseUser that allows the use of identity authentication at the user interface layer. The existence of this base user application class would allow, in the future, the creation of other system users who do not chat (They could, for example, have an administrative or management role)

### Business Logic Layer

This layer is oriented to everything related to the business logic of the application. It has an interface (IRepository <TEntity>) that represents a generic definition of the "Repository" design pattern for any entity. Then, there is a GenericRepository class that implements the previous interface for EntityFramework, allowing you to specify the entity of the EF Context to use. The generic repository can be extended, in order to perform specific implementations for a particular entity of any of its methods.

The repository implementation uses the AutoMapper extension, allowing the repository to have mapped versions of each of its methods. This makes it possible to easily convert the entities of the data model into classes of the view model specific to the user interface, directly when using the repository.

The generic repository also has events, which allow you to find out when the creation, modification or deletion of an entity occurs.

In turn, the "Unit of Work" design pattern is implemented to contain all available repositories, and allow them to share the Entity Framework context.

In this layer the implementation of the functionality that allows the connection with bots, which respond to user commands, is carried out. It was decided to implement the functionality in this layer, instead of performing it in the UI layer, to prevent it from being specific to the current UI. In this way, if new UIs are made in the future, they could use and add bot functionality to their interfaces.

This functionality has the following classes:

* MessageBotFactory: Static class that implements the "Factory" design pattern and allows to obtain the bot managers that know how to process the command received. It keeps a collection of available bot managers, that can be easily changed to obtain the list of bot managers from a xml/json configuration file.
* IMessageBot: Interface that represents a message bot manager. It has a method to validate that the command is suitable for this bot, and another to process the command.
* BotResponse: Represents the responses received from the bots.
* RabbitMQBot: A bot manager that implements the IMessageBot interface and allows you to connect to a specific bot through Remote Procedure Calls with RabbitMQ.
* NotSupportedCommandBot: In case the command is not suitable for any of the available bots, the MessageBotFactory is referred to this bot, which is responsible for notifying the user that the command could not be processed.
* BotResponsesObservable: Implementation of the "Observable" design pattern, which allows the user interface to subscribe to bot responses (which are done by publishing the responses by RabbitMQ in the "botresponses" fanout exchange. The intention behind the use of a Fanout exchange is to allow multiple instances of the business layer to run at the same time, being each of them able to be inform about the bots' responses)
* Unsubscriber <T>: Complementary class to BotResponsesObservable, which allows unsubscribing from the observable.
* BotResponsesHelper: Helper for sending responses to the Observable.

The functionality flow is explained below:

The commands arrive in the form of messages, so a specific implementation of the message repository messages was created, and the method responsible for adding new messages (Add) was overwritten. In this new method, if the message starts with "/", instead of adding it to the database, the repository sends it to the MessageBotFactory to start processing the command. The factory obtains the bot managers for which the command applies, and asks them to process it. If it cannot find a valid manager, it sends it to NotSupportedCommandBot. The corresponding bot manager interacts with the bot and asks it to process the command, indicating whether or not the bot started processing it. Once the bot finishes processing the command, it must send the command response(s) to the Fanout exchange "botresponses" that must exist on a RabbitMQ server running on the local computer. Those responses are received by the BotResponsesObservable, which informs all suscribers the response.

### MVC UI Layer

This layer implements a web user interface for the system, using the .NET MVC Framework.

It uses ASP.NET Identity to handle user authentication, saving the data in the same database that is being used for the chat application data model.

It has the following controllers defined:

* AccountController: Default controller for user registration and login management
* ManageController: Default controller for the management of the user account, by each user
* HomeController: Controller by default for the management of the "About" and "Contact" pages
* ChatroomController: Controller created to manage the actions of creation, modification, deletion and access to chat rooms.
* UserController: Specific controller for actions related to chat users. The default actions were created, but progress was not made with the creation of the views and their functionality, since they were not needed to meet the objectives of the Challenge.

It also has a Web Api controller for the management of chat messages (Api/MessageController).

ChatroomController, UserController and MessageController use the UOW of the business logic layer, and obtain it through a class (DI) implemented applying the "Dependency Injection" design pattern, which manages the obtaining of the UOW and the context of Signal R Hub for messages.

On the other hand, real-time communication and updating of chat rooms is done with Signal R, using the Message Hub defined in "/Signal R/MessageHub.cs"

It also has the definition of AutoMapper Mapper Profiles necessary for the use of project view models.

The flow of sending messages from the chat room is explained below:

1. At the moment when the user joins the chat room, the system joins the MessageHub group whose name is equal to the id of the chatroom, and subscribes to the BotResponses observable, defined in the business logic layer, through a single instance of the BotResponsesSuscriber created using the "Singleton" design pattern.
2. Once joined the group, the Hub sends all existing messages so far.
3. At the moment the user sends a message, it is received by the MessageController Web API controller, and is added using the message repository. If it is not a command, the repository adds it, and triggers the added entity event, which is captured by the UOW instance created in DI.cs, where the MessageHub is notified that a new message has been added. This then proceeds to notify all members of the corresponding group about the new message.
4. In case a command is sent, the flow is similar to the previous case, to the point where the repository receives the message. There, instead of adding it to the base, the repository starts the flow of command processing by the BotManagers.
5. When a response from a bot arrives, the BotResponsesSuscriber is informed about it, and asks the MessageHub to send the response to the corresponding chat room.

### Stooq Stock Message Bot Service

The implementation of the bot that reports the price of specific market shares was carried out through the creation of a Windows service. It can be installed on the computer, and configured to run automatically when the operating system boots.

Upon starting, the service starts an RPC server using RabbitMQ, which can receive the two instructions sent by the RabbitMQ Bot Manager created in the business logic layer: ValidateCommand and Process.

The validation that the command is suitable is done by checking if the command starts with "/stock=", and the processing of the command is carried out by means of the StooqStockInfoGetter class.

This class is created with the desired market share, and proceeds to consult Stooq's API for that share. Then, it returns a StooqStock collection with all valid quotes returned by the query.

Then, the bot sends, for each quotation obtained, a message to the local RabbitMQ Fanout exchange with the name "botresponses". In case of not having obtained any results, it informs that the share could not be found. The system captures any exceptions and informs it to the chat application using the same exchange.

### Prerequisites

The system needs the following packages to work: 

* [AutoMapper](https://www.nuget.org/packages/AutoMapper/9.0.0/) - For mapping between data and view models.
* [EntityFramework](https://www.nuget.org/packages/EntityFramework/6.4.0) - For database object mapping.
* [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/12.0.3) - Json serializing and deserializing
* [RabbitMQ.Client](https://www.nuget.org/packages/RabbitMQ.Client/5.1.2) - For connecting and interacting with the RabbitMQ Server
* [Bootstrap 3](https://www.nuget.org/packages/bootstrap/3.4.1) - For html and css view building
* [jQuery](https://www.nuget.org/packages/jQuery/3.3.1) - For javascript DOM manipulation
* [Signal R](https://www.nuget.org/packages/Microsoft.AspNet.SignalR/2.2.2) - For real time communication with web clients

Also, a RabbitMQ server (Tested with 3.8.2 version) should be installed on the localhost, which can be downloaded from [RabbitMQ Site](https://www.rabbitmq.com/download.html). For RabbitMQ Server to work, erlang should be installed too ([Erlang Site](https://www.erlang.org/download.html))


### Installing

The application was tested directly from the Visual Studio Environment. 

If SQL Server Express LocalDB is being used, it needs to have a folder named "App_Data" created at the project folder. The application will try to create the folder automatically if it does'nt exist at the global-asax file. 
If trying to run it under an IIS server, permissions should be granted to the applicationPool in order to allow the creation of the database files. 

The release folder contains two mains folders:

* WebPublish: Contains the publish content of the "Jobsity Web Chat" project. If using a IIS Server, this folder can be added to a IIS server as a .NET 4.5 Application. In order for the LocalDB file to be used, the application pool should be configured to load User Profile (loadUserProfile=true) and to set profile environment(setProfileEnvironment=true). If running from Visual Studio, those configurations are not needed.
* StooqBotService: Contains the compiled "StooqBotService" project. It needs to be installed as a service for the Stooq bot to work and process commands. To install it the following command can be used from a PowerShell with administrative rights, at the project main folder:

```
$currentPath = get-location
$fullPath = join-path -path $currentPath -childpath "Release\StooqBotService\StockBotService.exe"
New-Service -Name "JWCStockBotService" -BinaryPathName $fullPath
```

And then, to start it: 

```
Start-Service JWCStockBotService
```

With the service running, and the RabbitMQ server also running, the application should run correctly. In case the RabbitMQ server could not connect, a red alert will display when using the application.

## Running the tests

The functionality for which tests have been written is the Chatroom Repository from the Business Logic Layer. 

There are test methods that test the Add, Delete and Update methods, and the Repository events. 

The tests can be found in the JWC_BusinessModel.Tests Project.

## Authors

* **Lucas Marc** - *Full development* - [Email me](mailto:lucas.marc@gmail.com)